
#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

//14. range-based for Loop
int main(){
/* //ranged base for loop with vector
	vector<double> temps {11.3 , 53.35 , 34.42};
	double avg_tmp{};
	double total{};

	for (auto temp: temps)
		total += temp;

	avg_tmp = total /temps.size();
     cout << avg_tmp<< endl;
*/



/*
//Range - based for loop
int scores[] {18,28,38};
for ( int score : scores)
cout << score << endl;
///////////////////////////////////////////////////
return 0;
/*
//range base for loop (initalizer list )
 double avg_tmp{};
 double running_sum {};
 int size {0};

 for (auto temp: {1 , 2 ,3 ,4} ) {
 		running_sum += temp;
 		++size;
 }

avg_tmp = running_sum / size;

cout << avg_tmp ;
return 0;
*/
/////////////////////////////////////////



/*

//range base for loop (initalizer list )
 double avg_tmp{};
 double running_sum {};
 int size {0};

 for (auto temp: {1 , 2 ,3 ,4} ) {
 		running_sum += temp;
 		++size;
 }

avg_tmp = running_sum / size;

cout << avg_tmp ;
return 0;
*/
/////////////////////////////////////////

	//range base for loop (string)
//for (auto c: "Frank")
//	cout << c << endl;
////////////////////////////////////////
/*
///////ranged base for loop with vector

	vector<double> temps {11.3 , 53.35 , 34.42};
	double avg_tmp{};
	double total{};

	for (auto temp: temps) // temp:"2323asd"; 
		total += temp;

if (temps.size() != 0){
	avg_tmp = total / temps.size(); // #include <iomanip>
}
cout << fixed << setprecision(1);
	avg_tmp = total /temps.size();
     cout << avg_tmp<< endl;

   */
////////////////////////////////////
/* 


for (auto c: "this is test")
	if (c != ' ')
		cout <<c;
////////////////////////////////////////////
for (auto c: "this is test")
	if (c == ' ')
		cout <<"\t";
	else 
		cout << c;
*/




//
}