# sudo chmod +x language_switcher 
# language_switcher.sh

language_one="us"
language_two="ru"

current_layout=$(setxkbmap -query | grep layout | awk '{print $2}')

if [ "$current_layout" == "$language_one" ]; then
    echo "Switching to $language_two"
    setxkbmap "$language_two"
elif [ "$current_layout" == "$language_two" ]; then
    echo "Switching to $language_one"
    setxkbmap "$language_one"
else
    echo "Current layout is neither $language_one nor $language_two"
fi
