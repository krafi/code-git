#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    cout.setf(ios::fixed);
    double p1,p2,p3,p4,last;
    cin>>p1>>p2>>p3>>p4;
    double sum = ((p1 * 2 + p2 * 3 + p3 * 4 + p4) /10);
    cout<<fixed;
    cout <<setprecision(1)<<"Media: "<< sum<<endl;
    if(sum >= 7.0) {
        cout <<"Aluno aprovado."<<endl;
    }
    else if(sum >= 5.0) 
    {
        cout<<"Aluno em exame.\n";
        cin>>last;



        cout << setprecision(1)<<"Nota do exame: "<<last<<endl;

        if(last + sum / 2.0 > 5.0) {
            cout<<"Aluno aprovado.\n";
        }
        else{
            cout<<"Aluno reprovado.\n";
        }



        cout <<setprecision(1)<<"Media final: "<<(last + sum ) / 2.0<<endl;
    }
    else{
        cout << "Aluno reprovado."<<endl;
    }
    return 0;
}
