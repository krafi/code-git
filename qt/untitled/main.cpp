#include <QCoreApplication>
#include <animal.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    animal cat;
    animal dog;
    animal fish;
    animal rabbit;
    cat.setObjectName("kitty");
    dog.setObjectName("doggy");
    fish.setObjectName("fishhy");
    rabbit.setObjectName("rabbity");

    cat.speak("mewo");
    dog.speak("bark");
    fish.speak("blah");
    rabbit.speak("ka");

    return a.exec();
}
