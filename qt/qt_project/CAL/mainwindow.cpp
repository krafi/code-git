#include "mainwindow.h"
#include "ui_mainwindow.h"

double calcVal = 0.0;
bool divTrigger= false;
bool multTrigger= false;
bool addTrigger= false;
bool subTrigger= false;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    ui->DISPLAY->setText(QString::number(calcVal));
    QPushButton *numButtons[10];
    for(int i =0; i< 10; ++i){
        QString butName = "Button" + QString::number(i);
    numButtons[i] = MainWindow::findChild<QPushButton *>(butName);
    connect(numButtons[i] , SIGNAL(released()),this,
            SLOT(NumPressed()));
    }
    connect(ui->plus , SIGNAL(released()),this,
            SLOT(MathButtonPressed()));
    connect(ui->minus , SIGNAL(released()),this,
            SLOT(MathButtonPressed()));
    connect(ui->multiplay , SIGNAL(released()),this,
            SLOT(MathButtonPressed()));
    connect(ui->devide , SIGNAL(released()),this,
            SLOT(MathButtonPressed()));
    connect(ui->equal , SIGNAL(released()),this,
            SLOT(EqualButton()));
//    connect(ui->changesing , SIGNAL(released()),this,
//            SLOT(ChangeNumberSign()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::NumPressed(){
    QPushButton *button = (QPushButton *)sender();
    QString butval = button -> text();
    QString displayVal = ui -> DISPLAY -> text();
    if((displayVal.toDouble() == 0 ) ||(displayVal.toDouble() == 0.0 ) ){
        ui->DISPLAY->setText(butval);
    }
    else {
        QString newVal = displayVal + butval;
        double dblNewVal = newVal.toDouble();
        ui->DISPLAY->setText(QString::number(dblNewVal, 'g', 16));
    }
}
void MainWindow::MathButtonPressed(){

    divTrigger= false;
    multTrigger= false;
    addTrigger= false;
    subTrigger= false;
    QString displayval = ui->DISPLAY->text();
    calcVal = displayval.toDouble();
    QPushButton *button = (QPushButton *) sender();
    QString butval = button -> text();
    if (QString::compare(butval, "/", Qt::CaseSensitive) == 0){
        divTrigger = true;
    }
    else if (QString::compare(butval, "*", Qt::CaseSensitive) == 0){
        multTrigger = true;
    }
    else if (QString::compare(butval, "+", Qt::CaseSensitive) == 0){
        addTrigger = true;
    }
    else if (QString::compare(butval, "/", Qt::CaseSensitive) == 0){
        subTrigger = true;
    }
    ui -> DISPLAY -> setText("");

}
void MainWindow :: EqualButton(){
    double solution = 0.0;
    QString displayVal = ui ->DISPLAY->text();
    double dblDisplayVal = displayVal.toDouble();
    if(addTrigger || subTrigger || multTrigger || divTrigger){
        if (addTrigger){
            solution = calcVal + dblDisplayVal;
        }
        else if(subTrigger){
            solution = calcVal - dblDisplayVal;
        }
        else if(multTrigger){
            solution = calcVal * dblDisplayVal;

        }
        else{
            solution = calcVal / dblDisplayVal;
        }
    }
    ui->DISPLAY->setText(QString::number(solution));
}
/*
void MainWindow::ChangeNumberSign(){
    QString displayVal = ui->DISPLAY ->text();
    QRegularExpression Regular("[-]?[0-9.]*");
    if (reg.exactMatch(displayVal)){
        double dblDisplayVal = displayVal.toDouble();
        double dblDisplayValsign = -1 * dblDisplayVal;
        ui ->DISPLAY ->setText(QString::number(dblDisplayValsign));
    }
}

*/





















