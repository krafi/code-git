/********************************************************************************
** Form generated from reading UI file 'newhello.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWHELLO_H
#define UI_NEWHELLO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_newhello
{
public:
    QWidget *centralwidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *newhello)
    {
        if (newhello->objectName().isEmpty())
            newhello->setObjectName(QString::fromUtf8("newhello"));
        newhello->resize(800, 600);
        centralwidget = new QWidget(newhello);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        newhello->setCentralWidget(centralwidget);
        menubar = new QMenuBar(newhello);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        newhello->setMenuBar(menubar);
        statusbar = new QStatusBar(newhello);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        newhello->setStatusBar(statusbar);

        retranslateUi(newhello);

        QMetaObject::connectSlotsByName(newhello);
    } // setupUi

    void retranslateUi(QMainWindow *newhello)
    {
        newhello->setWindowTitle(QCoreApplication::translate("newhello", "newhello", nullptr));
    } // retranslateUi

};

namespace Ui {
    class newhello: public Ui_newhello {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWHELLO_H
